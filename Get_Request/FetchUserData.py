import requests
import json
import jsonpath

# Api URL
url = "https://reqres.in/api/users?page=2"

# Send Get request
response = requests.get(url)
print(response)

# Validate Status Code
assert response.status_code == 200

# Display Response Content
print(response.content)

# Parse response to Json format
json_response = json.loads(response.text)
print (json_response)

# Fetch value using Json Path - the pages object is a list type
pages = jsonpath.jsonpath(json_response, 'total_pages')
print(pages)
assert pages[0] == 4
